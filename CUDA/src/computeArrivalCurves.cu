#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <time.h>
#include <helper_string.h>

#define BILLION 1E9
#define NUM_BLOCKS 1
#define NUM_THREADS 1024

using namespace std;


void printVector (long n, vector<long>& vector) {
	for (int i = 0; i < n; ++i) {
		printf("%li ", vector[i]);
	}
	printf("\n");
}

__global__ void resetKeyEnd(long *KeyEnd, int N) {
	for (int i = blockDim.x * blockIdx.x + threadIdx.x; i < N; i += blockDim.x * gridDim.x) {
		KeyEnd[i] = -10;
    	}
}

__global__ void getQuantInt(ulong N, long *dTrace, long *dQuantInt, long pivot, long stepSize, ulong shift) {
	 //uint index = threadIdx.x;
   	 //uint stride = blockDim.x;
	 //for(uint id = index; id < N; id += stride) {
//		printf("id = %d, quant = %ld\n", blockIdx.x*blockDim.x+threadIdx.x, blockDim.x*gridDim.x);
	for (int id = blockIdx.x * blockDim.x + threadIdx.x; id < N; id += blockDim.x * gridDim.x) {
		dQuantInt[id] = (dTrace[id + shift] - pivot)/stepSize;
//		printf("id = %d, quant = %ld\n", id, dQuantInt[id]);
	}
}

__global__ void getBucketsAndUpdateUppGlobal(ulong N, long *dQuantInt, long *dUppGlobalCurve, long *dKeyEnd, long *dNextBucket) {
	int currentVal, previousVal;

	uint index = threadIdx.x;
  	uint stride = blockDim.x;
//	for(uint i = index; i < N; i += stride) {
	for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < N; i += blockDim.x * gridDim.x) {
		if (N == 1) {
			dKeyEnd[0] = -10;
		}

		if (i > 0) {
			currentVal = dQuantInt[i];
			previousVal = dQuantInt[i-1];
			if (currentVal != previousVal) {
				dKeyEnd[previousVal] = i - 1;
				dNextBucket[previousVal] = currentVal;
				if (dUppGlobalCurve[previousVal] < dKeyEnd[previousVal] + 1) {
					dUppGlobalCurve[previousVal] = dKeyEnd[previousVal] + 1;
				}
			}

			if (i == N-1) {
				dKeyEnd[currentVal] = -10; //i;
				dNextBucket[currentVal]= currentVal + 1;
    			if (dUppGlobalCurve[currentVal] < i+1) {
					dUppGlobalCurve[currentVal] = i+1;
    			}
			}
		}
	}
}

__global__ void updateUppGlobalR1(ulong N, long *dQuantInt, long *dUppGlobalCurve) {
	int currentVal, previousVal, localValue;

	uint index = threadIdx.x;
  	uint stride = blockDim.x;
//	for(uint i = index; i < N; i += stride) {
	for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < N; i += blockDim.x * gridDim.x) {
		if (N == 1) {
			localValue = 0;
		}

		if (i > 0) {
			currentVal = dQuantInt[i];
			previousVal = dQuantInt[i-1];
			if (currentVal != previousVal) {
				localValue = i-1;
				if (dUppGlobalCurve[previousVal] < localValue+1)
					dUppGlobalCurve[previousVal] = localValue+1;
			}

			if (i == N-1) {
				localValue = i;
    			if (dUppGlobalCurve[currentVal] < localValue+1) {
					dUppGlobalCurve[currentVal] = localValue+1;
    			}
			}
		}
	}
}

__global__ void updateLowGlobal(ulong N, long *dKeyEnd, long *dNextBucket, long *dLowGlobalCurve) {
	int firstIndex, lastIndex, localMin;
	dLowGlobalCurve[0] = 0;
	uint index = threadIdx.x;
  	uint stride = blockDim.x;
	// for(uint i = index; i < N; i += stride) {
	for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < N; i += blockDim.x * gridDim.x) {
		if (dKeyEnd[i] >= 0) {
			localMin = dKeyEnd[i];
			firstIndex = i+1;
			lastIndex = dNextBucket[i];
			if (dLowGlobalCurve[firstIndex] > localMin) {
				dLowGlobalCurve[firstIndex] = localMin;
			}

			if (firstIndex < lastIndex) {
				if (dLowGlobalCurve[lastIndex] > localMin)
					dLowGlobalCurve[lastIndex] = localMin;
			}
		}
	}
}

__global__ void updateLowGlobalR1(ulong N, long *dQuantInt, long *dLowGlobalCurve) {
	uint index = threadIdx.x;
  	uint stride = blockDim.x;
	// for(uint i = index; i < N; i += stride) {

	for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < N; i += blockDim.x * gridDim.x) {
		int firstIndex, lastIndex, localMin;
		localMin = i;
		firstIndex = dQuantInt[i];

		if (i == N-1) {
			lastIndex = firstIndex;
		} else {
			lastIndex = dQuantInt[i+1];
		}

		for(long j = firstIndex; j < lastIndex; ++j) {
			if (dLowGlobalCurve[j] > localMin) {
				dLowGlobalCurve[j] = localMin;
			}
		}
	}
}


int main(int argc, char **argv) {
	bool isVerbose = true;

	ulong N = 0;
	long stepSize = 1;
	long DtMax = -10;

	char *strFileName = NULL;

	vector<vector<long> > hTraceTotal;
    vector<long> valueLine;

    if (argc > 1) {
    	if (checkCmdLineFlag(argc, (const char **) argv, "N")) {
        	N = getCmdLineArgumentInt(argc, (const char **)argv, "N");
        }

     	if (checkCmdLineFlag(argc, (const char **) argv, "r")) {
        	stepSize = getCmdLineArgumentInt(argc, (const char **)argv, "r");
     	}

      	if (checkCmdLineFlag(argc, (const char **) argv, "f")) {
        	getCmdLineArgumentString(argc, (const char **)argv, "f", (char **) &strFileName);
      	}

      	if (checkCmdLineFlag(argc, (const char **) argv, "DtMax")) {
        	DtMax = getCmdLineArgumentInt(argc, (const char **)argv, "DtMax");
      	}

      	if (checkCmdLineFlag(argc, (const char **) argv, "verbose")) {
        	isVerbose = getCmdLineArgumentInt(argc, (const char **)argv, "verbose");
      	}
	}

	ifstream fin(strFileName);

	if (!fin) {
		printf("No valid input file... Aborting\n");
		return 1;
	}

    string item;
   	for (string line; getline(fin, line); ) {
        istringstream in(line);

    	while (getline(in, item, '\n')) {
        	valueLine.push_back(atof(item.c_str()));
    	}

    	hTraceTotal.push_back(valueLine);
    	valueLine.clear();
	}

	if (N == 0 || N > hTraceTotal.size()) {
    	N = hTraceTotal.size();
	}

	vector<long> hTrace(N);

	for (long j = 0; j < N; ++j) {
		hTrace[j] = hTraceTotal[j][0] - hTraceTotal[0][0] ;
		//printf("%d ", hTrace[j]);
		if (j > 0 && hTrace[j] < hTrace[j-1]) {
			printf("\n Error: Timestamps in trace file are out of order. TS[%li] < TS[%li]\n", j, j-1);
			return 1;
		}
	}

	if (DtMax == -10) {
		DtMax = hTrace[N-1] - hTrace[0];
	}

	long nBuckets = (DtMax / stepSize) + 1;

	vector<long> hQuantInt(N);
	vector<long> hUppGlobCurve(nBuckets, 0);
	vector<long> hLowGlobCurve(nBuckets, (N + 10));
	vector<long> hKeyEnd(nBuckets, -10);
	vector<long> hNextBucket(nBuckets);

	long hPivot;

	long* dTrace;
	cudaMalloc((void**)&dTrace, N * sizeof(long));
    cudaMemcpy(dTrace, hTrace.data(), N * sizeof(long), cudaMemcpyHostToDevice);

	long* dQuantInt;
	cudaMalloc((void**)&dQuantInt, N * sizeof(long));
    // cudaMemcpy(dQuantInt, hQuantInt, N * sizeof(long), cudaMemcpyHostToDevice);

	long* dUppGlobCurve;
	cudaMalloc((void**)&dUppGlobCurve, nBuckets * sizeof(long));
    cudaMemcpy(dUppGlobCurve, hUppGlobCurve.data(), nBuckets * sizeof(long), cudaMemcpyHostToDevice);

	long* dLowGlobCurve;
	cudaMalloc((void**)&dLowGlobCurve, nBuckets * sizeof(long));
    cudaMemcpy(dLowGlobCurve, hLowGlobCurve.data(), nBuckets * sizeof(long), cudaMemcpyHostToDevice);

	long* dKeyEnd;
	cudaMalloc((void**)&dKeyEnd, nBuckets * sizeof(long));
    cudaMemcpy(dKeyEnd, hKeyEnd.data(), nBuckets * sizeof(long), cudaMemcpyHostToDevice);

	long* dNextBucket;
	cudaMalloc((void**)&dNextBucket, nBuckets * sizeof(long));
    //cudaMemcpy(dNextBucket, hNextBucket, nBuckets * sizeof(long), cudaMemcpyHostToDevice);

	int threadsPerBlock = 1024;

	struct timespec start, stop;
  	clock_gettime(CLOCK_REALTIME, &start);

	for (long i = 0; i < N; ++i) {
		hPivot = hTrace[i];

		int nBlocksNi = (((N-i) + NUM_THREADS - 1) / NUM_THREADS);
		int nBlocksBuckets = ((nBuckets + NUM_THREADS - 1) / NUM_THREADS);

		getQuantInt<<<nBlocksNi, NUM_THREADS>>>(N-i, dTrace, dQuantInt, hPivot, stepSize, i);
		//cudaMemcpy(hQuantInt.data(), dQuantInt, N * sizeof(long), cudaMemcpyDeviceToHost);
		//for (int k = 0; k < N; ++k) {
		//	printf("%ld ", hQuantInt[k]);
		//}
		//printf("\n");

		if(stepSize == 1) {
			updateUppGlobalR1<<<nBlocksNi, NUM_THREADS>>>(N-i, dQuantInt, dUppGlobCurve);
			updateLowGlobalR1<<<nBlocksNi, NUM_THREADS>>>(N-i, dQuantInt, dLowGlobCurve);
		} else {
			//cudaMemcpy(dKeyEnd, hKeyEnd.data(), nBuckets * sizeof(long), cudaMemcpyHostToDevice);
			resetKeyEnd<<<nBlocksBuckets, NUM_THREADS>>>(dKeyEnd, nBuckets);
			getBucketsAndUpdateUppGlobal<<<nBlocksNi, NUM_THREADS>>>(N-i, dQuantInt, dUppGlobCurve, dKeyEnd, dNextBucket);
			updateLowGlobal<<<nBlocksBuckets, NUM_THREADS>>>(nBuckets, dKeyEnd, dNextBucket, dLowGlobCurve);
		}
	}

	cudaDeviceSynchronize();

	cudaMemcpy(hUppGlobCurve.data(), dUppGlobCurve, nBuckets * sizeof(long), cudaMemcpyDeviceToHost);
	cudaMemcpy(hLowGlobCurve.data(), dLowGlobCurve, nBuckets * sizeof(long), cudaMemcpyDeviceToHost);

	for (long i = 1; i < nBuckets; ++i) {
		if (hUppGlobCurve[i] == 0 || hUppGlobCurve[i] < hUppGlobCurve[i-1]) {
			hUppGlobCurve[i] = hUppGlobCurve[i-1];
		}
	}

    for(long i = nBuckets-1; i > 0; --i) {
		if (hLowGlobCurve[i-1] > hLowGlobCurve[i]) {
			hLowGlobCurve[i-1] = hLowGlobCurve[i];
		}
	}

	clock_gettime(CLOCK_REALTIME, &stop);
	double accum;
	accum = (stop.tv_sec - start.tv_sec) + (double)((stop.tv_nsec - start.tv_nsec) / BILLION);

//	if (isVerbose != 0) {
//		for (long i=0; i < nBuckets; ++i) {
//			printf("%li, %li, %li\n", i*stepSize, hLowGlobCurve[i], hUppGlobCurve[i]);
//		}
//	}
	printf("%li, %li, %f\n", N, stepSize, accum);

}
