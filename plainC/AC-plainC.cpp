#include "AC_headers.h"
#include <iterator>
#include <time.h>

#define DEBUG 0
#define BILLION 1E9

void printVector(long n, vector<long>& vector) {
    for (int i = 0; i < n; ++i) {
        printf("%li ", vector[i]);
    }
    printf("\n");
}

vector<vector<long> > computeArrivalCurve(vector<long> h_trace, const unsigned long stepSize, const unsigned long maxN, const long pDtMax) {
    // Use upper bound if one was defined
    unsigned long N = maxN;
    long DtMax = pDtMax;

    if (N == 0 || N> h_trace.size()) {
        N = h_trace.size();
    }

    // restrict elements to upper bound
    h_trace.resize(N);

    //Substract initial offset
    unsigned long tmp = h_trace.front();

    for (long &d : h_trace) {
        d -= tmp;
    }

    // if not provided as an argument, use default timespan
    if (DtMax < 0) {
        DtMax = h_trace[N-1] - h_trace[0];
    }

    long nBuckets = (DtMax/stepSize) + 1;

    vector<long> h_QuantInt(N);
    vector<long> h_UppGlobalCurve(nBuckets);
    vector<long> h_LowGlobalCurve(nBuckets);
    vector<long> h_sum(nBuckets, 0);
    vector<long> h_total(nBuckets, 0);
    vector<float> h_meanCurve(nBuckets);
    fill(h_LowGlobalCurve.begin(), h_LowGlobalCurve.end(), N + 10);

    vector<long> h_KeyEnd(nBuckets);
    vector<long> h_NextBucket(nBuckets);

    long h_pivot;

    struct timespec start, stop;
    clock_gettime(CLOCK_REALTIME, &start);


    for (unsigned int i = 0; i < N; ++i) {
        h_pivot = h_trace[i];
        getQuantInt(N-i, h_trace, h_QuantInt, h_pivot, stepSize, i);
        if (stepSize == 1) {
            UpdateUpperGlobalR1(N-i, h_QuantInt, h_UppGlobalCurve);
            UpdateLowerGlobalR1(N-i, h_QuantInt, h_LowGlobalCurve);
        } else {
            fill(h_KeyEnd.begin(),h_KeyEnd.end(),-10);

            GetBucketsAndUpdateUppGlobal(N-i, h_QuantInt, h_UppGlobalCurve, h_KeyEnd, h_NextBucket);
            UpdateLowGlobal(nBuckets, h_KeyEnd, h_NextBucket, h_LowGlobalCurve);
            UpdateSumAndTotal(N-i, h_QuantInt, h_sum, h_total);
        }
    }

    FillFinalUpperCurve(h_UppGlobalCurve);

    for (unsigned int i = nBuckets-1; i > 0; --i) {
        h_LowGlobalCurve[i-1] = min(h_LowGlobalCurve[i-1], h_LowGlobalCurve[i]);
    }

    for (int i = 0; i < nBuckets; ++i) {
        if (h_total[i] != 0) {
            h_meanCurve[i] = float(h_sum[i])/float(h_total[i]);
        } else {
            h_meanCurve[i] = float(h_LowGlobalCurve[i] + h_UppGlobalCurve[i])/float(2);
        }
    }

    clock_gettime(CLOCK_REALTIME, &stop);
    double accum;
    accum = ( stop.tv_sec - start.tv_sec ) + (double)(( stop.tv_nsec - start.tv_nsec ) / BILLION);

    //printf("eventWindows, eventMin, eventMean, eventMax \n");
    //for (unsigned int i = 0; i < h_LowGlobalCurve.size(); ++i) {
    //   printf("%ld, %ld, %f, %ld\n", i*stepSize, h_LowGlobalCurve[i], h_meanCurve[i], h_UppGlobalCurve[i]);
    //}
    printf("%li, %li, %f\n", N, stepSize, accum);

    vector<vector<long> > res = {h_UppGlobalCurve, h_LowGlobalCurve};
    return res;
}

int main(int argc, char **argv) {
    size_t N = 0;
    bool verbose = 1;
    long stepSize = 1;
    long DtMax = -10;
    long i, j, k;
    char *in_fname = NULL;
    if (argc > 1) {
        if (checkCmdLineFlag(argc, (const char **) argv, "N")) {
            N = getCmdLineArgumentInt(argc, (const char **)argv, "N");
        }

        if (checkCmdLineFlag(argc, (const char **) argv, "r")) {
            stepSize = getCmdLineArgumentInt(argc, (const char **)argv, "r");
        }

        if (checkCmdLineFlag(argc, (const char **) argv, "f")) {
            getCmdLineArgumentString(argc, (const char **)argv, "f", (char **) &in_fname);
        }

        if (checkCmdLineFlag(argc, (const char **) argv, "DtMax")) {
            DtMax = getCmdLineArgumentInt(argc, (const char **)argv, "DtMax");
        }

        if (checkCmdLineFlag(argc, (const char **) argv, "verbose")) {
            verbose = getCmdLineArgumentInt(argc, (const char **)argv, "verbose");
        }
    }

    ifstream fin(in_fname);
    if (!fin) {
        printf("No valid input file... Aborting\n");
        return 1;
    }

    vector<vector<long> > h_trace_total;
    vector<long> valueline;
    string item;
    for (string line; getline(fin, line); ) {
        istringstream in(line);
        while (getline(in, item, '\n')) {
            valueline.push_back(atof(item.c_str()));
        }

        h_trace_total.push_back(valueline);
        valueline.clear();
    }

    if (N == 0 || N> h_trace_total.size()) {
        N = h_trace_total.size();
    }

    vector<long> h_trace(N);

    //Substract base here and verify whether it's sorted. Sanity check. Exit if not passed.
    for (j = 0; j < N; ++j) {
        h_trace[j] = h_trace_total[j][0] - h_trace_total[0][0] ;
        if ( j > 0 && h_trace[j] < h_trace[j-1]) {
            printf("\n Error: Timestamps in trace file are out of order. TS[%li] < TS[%li]\n", j, j-1);
            return 1;
        }
    }

    vector<vector<long> > res;
    res = computeArrivalCurve(h_trace, stepSize, N, DtMax);

    vector<long> v_upp = res[0];
    vector<long> v_low = res[1];

    return 0;
}
