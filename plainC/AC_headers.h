#ifndef __AC_HEADERS_H__
#define __AC_HEADERS_H__

#include <string>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <iostream>     // std::cout
#include <sstream>
#include <vector>
#include <helper_string.h>
#include <boost/timer/timer.hpp>

using namespace std;

// Generate the final curve by filling the spaces that may have not appeared (and then are not updated) during the iterations
void FillFinalUpperCurve(vector<long>& UpperCurve) {
    int i;
    for (i = 1; i < UpperCurve.size(); ++i) {
        if (UpperCurve[i] == 0 || UpperCurve[i] < UpperCurve[i-1]) {
            UpperCurve[i] = UpperCurve[i-1];
        }
    }
}

// Compute the discrete offsets
void getQuantInt(const long n, const vector<long>& trace, vector<long>& QuantInt, const long pivot, const long resolution, const long shift) {
    for (int i = 0; i < n; ++i) {
        QuantInt[i] = (trace[i + shift] - pivot)/resolution;
    }
}


void GetBucketsAndUpdateUppGlobal(const long n, const vector<long>& QuantInt, vector<long>& UpperGlobalCurve, vector<long>& KeyEnd, vector<long>& NextBucket) {
    long current_val, previous_val;

    if (n == 1) {
        KeyEnd[0] = -10;
    }

    for (int i = 1; i < n; ++i)	{
        current_val  = QuantInt[i];
        previous_val = QuantInt[i-1];

        if (current_val != previous_val) {

            KeyEnd[previous_val] = i-1;
            NextBucket[previous_val] = current_val;
            UpperGlobalCurve[previous_val] = max(UpperGlobalCurve[previous_val], KeyEnd[previous_val]+1);
        }

        if (i == n-1) {
            KeyEnd[current_val] = -10; //i;
            NextBucket[current_val]= current_val+1;
            UpperGlobalCurve[current_val] = max(UpperGlobalCurve[current_val], (long int)i+1);
        }
    }
}

void UpdateSumAndTotal(const long n, const vector<long>& QuantInt, vector<long>& sumCount, vector<long>& totalCount) {
    for (int i = 1; i < n; ++i)	{
        sumCount[QuantInt[i]] += i;
        totalCount[QuantInt[i]] += 1;
    }
}

void UpdateLowGlobal(const long n, const vector<long>& KeyEnd, vector<long>& NextBucket, vector<long>& LowerGlobalCurve) {
    int i, j, first_index, last_index;
    long LocalMin;
    LowerGlobalCurve[0] = 0;

    for (i = 0; i < n-1; ++i)	{
        if(KeyEnd[i] >= 0) {
            LocalMin = KeyEnd[i];
            first_index = i+1;
            last_index = NextBucket[i];

            LowerGlobalCurve[first_index]= min(LowerGlobalCurve[first_index], LocalMin);

            if(first_index < last_index) {
	           LowerGlobalCurve[last_index]= min(LowerGlobalCurve[last_index], LocalMin);
            }
        }
    }
}

// These functions handle the case when stepSize = 1.
// Must be handled separately because it doesn't require quantization and the results must match the reference functions from RTC toolbox
// Basically, when r=1 each offsets should appear in the QuantInt vector at most one time, so there is nothing to reduce or preprocess.
// The curve is updated directly using the offset values.
void UpdateLowerGlobalR1(long n, const vector<long>& QuantInt, vector<long>& LowerGlobalCurve) {
    int i, j, first_index, last_index;
    long LocalMin;

    for (i = 0; i < n; ++i) {
        LocalMin = i;
        first_index = QuantInt[i];
        if (i == n-1) {
            last_index = first_index;
        } else {
            last_index = QuantInt[i+1];
        }

        //This is the key here for R=1
        for(j = first_index; j < last_index; ++j) {
            LowerGlobalCurve[j] = min(LowerGlobalCurve[j], LocalMin);
        }
    }
}

void UpdateUpperGlobalR1(long n, vector<long>& QuantInt, vector<long>& UpperGlobalCurve) {
    int i;
    long current_val, previous_val, value;

    if(n == 1) {
        value = 0;
    }

    for (i = 1; i < n; ++i) {
        current_val  = QuantInt [i];
        previous_val = QuantInt [i-1];

        if(current_val != previous_val) {
            value = i-1;
            UpperGlobalCurve[previous_val] = max(UpperGlobalCurve[previous_val], value+1);
        }

        if(i == n-1) {
            value = i;
            UpperGlobalCurve[current_val] = max(UpperGlobalCurve[current_val], value+1);
        }
    }
}

#endif // __AC_HEADERS_H__
