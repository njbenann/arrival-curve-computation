#output
TARGET  := AC-plainC

CPPOBJDIR = cppobjs

#wildcard expansion does not normally take place when a variable is set, or inside the arguments of a function. If you want to do wildcard expansion in such places, you need to use the wildcard function, like this:  $(wildcard pattern…)
#This string is replaced by a space-separated list of names of existing files that match one of the given file name patterns. If no existing file name matches a pattern, then that pattern is omitted from the output of the wildcard function. 
# Get list of all CPP source files in a directory
CPPSOURCES := $(wildcard *.cpp)

CPPOBJS := $(CPPSOURCES:%.cpp=$(CPPOBJDIR)/%.o)

COBJDIR = cobjs

CSOURCES := $(wildcard *.c)
COBJS := $(CSOURCES:%.c=$(COBJDIR)/%.o)

OBJS = $(CPPOBJS) $(COBJS)

OPT_FLAGS   := -fno-strict-aliasing -O3

# INC := -I ~/R/x86_64-pc-linux-gnu-library/3.3/Rcpp/include/ -I /usr/include/R -I.
INC := -I.

# LIB := -lboost_system -lboost_timer  -L /usr/lib/R/lib/ -lR /home/sfischme/R/x86_64-pc-linux-gnu-library/3.3/Rcpp/libs/Rcpp.so
LIB := -lboost_system -lboost_timer

CXXFLAGS := -m64 -std=c++11 -DUNIX $(WARN_FLAGS) $(OPT_FLAGS) $(INC)
CFLAGS := -m64 -DUNIX $(WARN_FLAGS) $(OPT_FLAGS) $(INC)

LDFLAGS := $(LIBRARY_PATH) $(LIB) 

.PHONY: clean

#target:  dependency1 dependency2 ...
#      <tab> command
#Example: 
#program: program.o mylib.o
#	gcc -o program program.o mylib.o

#-o file
#Place output in file file. This applies regardless to whatever sort of output is being produced, whether it be an executable file, an object file, an assembler file or preprocessed C code. If not specified, the default is `a.out'

# When you invoke GCC , it normally does preprocessing, compilation, assembly and linking. The "overall options" allow you to stop this process at an intermediate stage. For example, the -c option says not to run the linker. Then the output consists of object files output by the assembler. 

#    % g++ -c -o foo.o foo.C 
#    % g++ -c -o main.o main.C
#    % g++ -c -o bar.o bar.C
#    % g++ -o fubar foo.o main.o bar.o -lm
#The first three commands generate foo.o, main.o and bar.o respectively. The last line links them together along with the math library, libm.a. 

$(TARGET): $(CPPOBJDIR) $(COBJDIR) $(CPPOBJS) $(COBJS)
	g++ -fPIC -o $(TARGET) $(CPPOBJS) $(LDFLAGS) 

#$@ : The file name of the target of the rule. If the target is an archive member, then ‘$@’ is the name of the archive file. In a pattern rule that has multiple targets ‘$@’ is the name of whichever target caused the rule’s recipe to be run. 

#$< : The name of the first prerequisite. If the target got its recipe from an implicit rule, this will be the first prerequisite added by the implicit rule

$(CPPOBJS): $(CPPOBJDIR)/%.o: %.cpp
	@echo "compile $@ $<"
	g++ -c -fPIC $(CXXFLAGS) $< -o $@



#%.o : %.c ; recipe…
#specifies how to make a file n.o, with another file n.c as its prerequisite, provided that n.c exists or can be made.         

$(COBJS): $(COBJDIR)/%.o: %.c
	@echo "compile $@ $<"
	gcc -c -fPIC $(CFLAGS) $< -o $@
        
$(CPPOBJDIR):   
	@ mkdir -p $(CPPOBJDIR)

$(COBJDIR):     
	@ mkdir -p $(COBJDIR)

clean:
	$(RM) $(TARGET) $(OBJ)
	$(RM) -rf $(CPPOBJDIR)
	$(RM) -rf $(COBJDIR)
	$(RM) *~

