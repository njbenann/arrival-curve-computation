__kernel __attribute__ ((reqd_work_group_size(1, 1, 1))) void GetBucketsAndUpdateUppGlobal(int n,
																							global int *QuantInt,
																							global int *UppGlobalCurve,
																							global int *KeyEnd,
																							global int *NextBucket) {
	int current_val, previous_val;
//	__attribute__((opencl_unroll_hint))
	for (int i = get_global_id(0); i < n; i += get_global_size(0)) {
		if (n == 1) {
			KeyEnd[0] = -10;
		}
	
		if (i > 0) {				
			current_val = QuantInt[i];
			previous_val = QuantInt[i-1];
			if (current_val != previous_val) {
				KeyEnd[previous_val] = i-1;
				NextBucket[previous_val] = current_val;
				if (UppGlobalCurve[previous_val] < KeyEnd[previous_val]+1) {
					UppGlobalCurve[previous_val] = KeyEnd[previous_val]+1;
				}
			}
	
			if (i == n-1) {
				KeyEnd[current_val] = -10; // i;
				NextBucket[current_val]= current_val+1;
				if (UppGlobalCurve[current_val] < i+1) {
					UppGlobalCurve[current_val] = i+1;
				}
			}
		}
	}
}
