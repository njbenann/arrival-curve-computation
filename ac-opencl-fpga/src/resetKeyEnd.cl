__kernel __attribute__ ((reqd_work_group_size(1, 1, 1))) void resetKeyEnd(global int *keyEnd, int allocSize) {
//	__attribute__((opencl_unroll_hint))
    for (int i = get_global_id(0); i < allocSize; i+=get_global_size(0)) {
		keyEnd[i] = -10;
	}
}
