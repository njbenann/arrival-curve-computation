__kernel __attribute__ ((reqd_work_group_size(1, 1, 1))) void UpdateUppGlobalR1(int n, global int *QuantInt, global int *UppGlobalCurve) {
	int current_val, previous_val, local_value;

	for(int i = get_global_id(0); i < n; i += get_global_size(0)) {
		if (n == 1) {
			local_value = 0;
		}
	
		if (i > 0) {				
			current_val = QuantInt[i];
			previous_val = QuantInt[i-1];
			if (current_val != previous_val) {
				local_value = i-1;
				if (UppGlobalCurve[previous_val] < local_value+1) {
					UppGlobalCurve[previous_val] = local_value+1;
				}
			}
	
			if (i == n-1) {
				local_value = i;
				if (UppGlobalCurve[current_val] < local_value+1) {
					UppGlobalCurve[current_val] = local_value+1;
				}
			}
		}
	}
}
