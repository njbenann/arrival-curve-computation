__kernel __attribute__ ((reqd_work_group_size(1, 1, 1))) void getQuantInt(int n, global long *trace, global int *QuantInt, long pivot, int stepSize, int shift) {
//	__attribute__((opencl_unroll_hint))
	for(int id = get_global_id(0); id < n; id += get_global_size(0)) {
//		printf("id = %d, n = %d, trace[id + shift] = %d, pivot = %d, stepSize = %d\n", id, n, trace[id + shift], pivot, stepSize);
    	QuantInt[id] = (trace[id + shift] - pivot)/stepSize;
	}
}
