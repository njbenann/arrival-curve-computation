__kernel __attribute__ ((reqd_work_group_size(1, 1, 1))) void UpdateLowGlobalR1(int n, global int *QuantInt, global int *LowGlobalCurve) {
	int first_index, last_index, LocalMin;
	int i, j;

	for (i = get_global_id(0); i < n; i += get_global_size(0)) {
		LocalMin = i;
		first_index = QuantInt[i];
			
		if (i == n-1) {
			last_index = first_index;
		} else {
			last_index = QuantInt[i+1];
		}
			
		for (j = first_index; j < last_index; ++j) {
			if (LowGlobalCurve[j] > LocalMin) {
				LowGlobalCurve[j] = LocalMin;					
			}
		}
	}
}
