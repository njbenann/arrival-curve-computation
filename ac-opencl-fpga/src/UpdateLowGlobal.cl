__kernel __attribute__ ((reqd_work_group_size(1, 1, 1))) void UpdateLowGlobal(int n, global int *KeyEnd, global int *NextBucket, global int *LowGlobalCurve) {
	int first_index, last_index, LocalMin;

	LowGlobalCurve[0] = 0;
//	__attribute__((opencl_unroll_hint))
	for (int i = get_global_id(0); i < n; i += get_global_size(0)) {
		if (KeyEnd[i] >= 0) {
			LocalMin = KeyEnd[i];
			first_index = i+1;
			last_index = NextBucket[i];
			if (LowGlobalCurve[first_index] > LocalMin) {
				LowGlobalCurve[first_index] = LocalMin;
			}
		
			if (first_index < last_index) {
				if (LowGlobalCurve[last_index] > LocalMin) {
					LowGlobalCurve[last_index] = LocalMin;
				}
			}
		}
	}
}
