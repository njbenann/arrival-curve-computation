#include "xcl2.hpp"

#include <iostream> 
#include <vector>
#include <algorithm>
#include <fstream>
#include <utility>
#include <sstream>
#include <string>

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>

#define BILLION 1E9

#define DEBUG 0
#define SUCCESS 1
#define FAILURE 0

#define OCL_CHECK(error,call)                                       \
    call;                                                           \
    if (error != CL_SUCCESS) {                                      \
      printf("%s:%d Error calling " #call ", error code is: %d\n",  \
              __FILE__,__LINE__, error);                            \
      exit(EXIT_FAILURE);                                           \
}

using namespace std;

void printVector (long n, vector<long>& vector) {
    for (int i = 0; i < n; ++i) {
        printf("%li ", vector[i]);
    }
    printf("\n");
}

int processTrace(vector<long, aligned_allocator<long>> &h_trace, const int& N, const int& r) {
    int DtMax = h_trace[N-1] - h_trace[0];

    int nBuckets = (DtMax / r) + 1;

    vector<int, aligned_allocator<int>> h_QuantInt(N, -10);
    vector<int, aligned_allocator<int>> h_UppGlobCurve(nBuckets);
    vector<int, aligned_allocator<int>> h_LowGlobCurve(nBuckets, N + 10);
    vector<int, aligned_allocator<int>> h_KeyEnd(nBuckets);
    vector<int, aligned_allocator<int>> h_NextBucket(nBuckets);
    
    int int_global_size = 4;
    int int_local_size = 1;

    long nBases = N;
    vector<int, aligned_allocator<int>>nBasesPerCompute(int_global_size);

    int nBasesSoFar = 0;
    for(int i = 0; i < int_global_size; ++i) {
        if (nBasesSoFar >= nBases) {
            nBasesPerCompute[i] = 0;
        }

        nBasesPerCompute[i] = nBases / int_global_size;
        if(i < (nBases % int_global_size)) {
            nBasesPerCompute[i]++;
        }
        nBasesSoFar += nBasesPerCompute[i];
    }

    cl::NDRange local = int_local_size;
    cl::NDRange global_size = int_global_size;

    cl_int ret;
    vector<cl::Device> devices = xcl::get_xil_devices();
    cl::Device device = devices[0];

    OCL_CHECK(ret, cl::Context context(device, NULL, NULL, NULL, &ret));
    OCL_CHECK(ret, cl::CommandQueue q(context, device, CL_QUEUE_PROFILING_ENABLE, &ret));
    OCL_CHECK(ret, string device_name = device.getInfo<CL_DEVICE_NAME>(&ret));

    string xclbinFile = xcl::find_binary_file(device_name, "container");
    cl::Program::Binaries bins = xcl::import_binary_file(xclbinFile);
    devices.resize(1);

    OCL_CHECK(ret, cl::Program program(context, devices, bins, NULL, &ret));
    OCL_CHECK(ret, cl::Kernel getQuantIntKrnl(program, "getQuantInt", &ret));
    OCL_CHECK(ret, cl::Kernel GetBucketsAndUpdateUppGlobalKrnl(program, "GetBucketsAndUpdateUppGlobal", &ret));
    OCL_CHECK(ret, cl::Kernel UpdateLowGlobalKrnl(program, "UpdateLowGlobal", &ret));
//    OCL_CHECK(ret, cl::Kernel UpdateUppGlobalR1Krnl(program, "UpdateUppGlobalR1", &ret));
//    OCL_CHECK(ret, cl::Kernel UpdateLowGlobalR1Krnl(program, "UpdateLowGlobalR1", &ret));
    OCL_CHECK(ret, cl::Kernel resetKeyEndKrnl(program, "resetKeyEnd", &ret));

    OCL_CHECK(ret, cl::Buffer d_trace(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, N * sizeof(long), h_trace.data(), &ret));
    OCL_CHECK(ret, cl::Buffer d_QuantInt(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, N * sizeof(int), h_QuantInt.data(), &ret));
    OCL_CHECK(ret, cl::Buffer d_UppGlobCurve(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, nBuckets * sizeof(int), h_UppGlobCurve.data(), &ret));
	OCL_CHECK(ret, cl::Buffer d_LowGlobCurve(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, nBuckets * sizeof(int), h_LowGlobCurve.data(), &ret));
	OCL_CHECK(ret, cl::Buffer d_KeyEnd(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, nBuckets * sizeof(int), h_KeyEnd.data(), &ret));
	OCL_CHECK(ret, cl::Buffer d_NextBucket(context, CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE, nBuckets * sizeof(int), h_NextBucket.data(), &ret));

	std::vector<cl::Memory> inBufVec, outBufVec;
	inBufVec.push_back(d_trace);
	inBufVec.push_back(d_QuantInt);
	inBufVec.push_back(d_UppGlobCurve);
	inBufVec.push_back(d_LowGlobCurve);
	inBufVec.push_back(d_KeyEnd);
	inBufVec.push_back(d_NextBucket);
	
	outBufVec.push_back(d_UppGlobCurve);
	outBufVec.push_back(d_LowGlobCurve);

    long h_pivot;

    struct timespec start, stop, enqueue_start, enqueue_stop;
    clock_gettime(CLOCK_REALTIME, &start);

    clock_gettime(CLOCK_REALTIME, &enqueue_start);
    OCL_CHECK(ret, ret = q.enqueueMigrateMemObjects(inBufVec, 0));

    int n_arg = 0;

    printf("Starting for %d\n", N);
    if (r == 1) {
//        for (int i = 0; i < N; ++i) {
//            h_pivot = h_trace[i];
//
//            n_arg = 0;
//            OCL_CHECK(ret, ret = getQuantIntKrnl.setArg(n_arg++, N-i));
//            OCL_CHECK(ret, ret = getQuantIntKrnl.setArg(n_arg++, d_trace));
//            OCL_CHECK(ret, ret = getQuantIntKrnl.setArg(n_arg++, d_QuantInt));
//            OCL_CHECK(ret, ret = getQuantIntKrnl.setArg(n_arg++, h_pivot));
//            OCL_CHECK(ret, ret = getQuantIntKrnl.setArg(n_arg++, r));
//            OCL_CHECK(ret, ret = getQuantIntKrnl.setArg(n_arg++, i));
//            OCL_CHECK(ret, ret = q.enqueueNDRangeKernel(getQuantIntKrnl, 0, global_size, local, NULL, NULL));
//
//            n_arg = 0;
//            OCL_CHECK(ret, ret = UpdateUppGlobalR1Krnl.setArg(n_arg++, N-i));
//            OCL_CHECK(ret, ret = UpdateUppGlobalR1Krnl.setArg(n_arg++, d_QuantInt));
//            OCL_CHECK(ret, ret = UpdateUppGlobalR1Krnl.setArg(n_arg++, d_UppGlobCurve));
//            OCL_CHECK(ret, ret = q.enqueueNDRangeKernel(UpdateUppGlobalR1Krnl, 0, global_size, local, NULL, NULL));
//
//            n_arg = 0;
//            OCL_CHECK(ret, ret = UpdateLowGlobalR1Krnl.setArg(n_arg++, N-i));
//            OCL_CHECK(ret, ret = UpdateLowGlobalR1Krnl.setArg(n_arg++, d_QuantInt));
//            OCL_CHECK(ret, ret = UpdateLowGlobalR1Krnl.setArg(n_arg++, d_LowGlobCurve));
//            OCL_CHECK(ret, ret = q.enqueueNDRangeKernel(UpdateLowGlobalR1Krnl, 0, global_size, local, NULL, NULL));
//        }
    } else {
        for (int i = 0; i < N; ++i) {
            h_pivot = h_trace[i];

            n_arg = 0;
            OCL_CHECK(ret, ret = getQuantIntKrnl.setArg(n_arg++, N-i));
            OCL_CHECK(ret, ret = getQuantIntKrnl.setArg(n_arg++, d_trace));
            OCL_CHECK(ret, ret = getQuantIntKrnl.setArg(n_arg++, d_QuantInt));
            OCL_CHECK(ret, ret = getQuantIntKrnl.setArg(n_arg++, h_pivot));
            OCL_CHECK(ret, ret = getQuantIntKrnl.setArg(n_arg++, r));
            OCL_CHECK(ret, ret = getQuantIntKrnl.setArg(n_arg++, i));
            OCL_CHECK(ret, ret = q.enqueueNDRangeKernel(getQuantIntKrnl, 0, global_size, local, NULL, NULL));

            n_arg = 0;
            OCL_CHECK(ret, ret = resetKeyEndKrnl.setArg(n_arg++, d_KeyEnd));
            OCL_CHECK(ret, ret = resetKeyEndKrnl.setArg(n_arg++, nBuckets));
            OCL_CHECK(ret, ret = q.enqueueNDRangeKernel(resetKeyEndKrnl, 0, global_size, local, NULL, NULL));

            n_arg = 0;
            OCL_CHECK(ret, ret = GetBucketsAndUpdateUppGlobalKrnl.setArg(n_arg++, N-i));
            OCL_CHECK(ret, ret = GetBucketsAndUpdateUppGlobalKrnl.setArg(n_arg++, d_QuantInt));
            OCL_CHECK(ret, ret = GetBucketsAndUpdateUppGlobalKrnl.setArg(n_arg++, d_UppGlobCurve));
            OCL_CHECK(ret, ret = GetBucketsAndUpdateUppGlobalKrnl.setArg(n_arg++, d_KeyEnd));
            OCL_CHECK(ret, ret = GetBucketsAndUpdateUppGlobalKrnl.setArg(n_arg++, d_NextBucket));
            OCL_CHECK(ret, ret = q.enqueueNDRangeKernel(GetBucketsAndUpdateUppGlobalKrnl, 0, 3, local, NULL, NULL));

            n_arg = 0;
            OCL_CHECK(ret, ret = UpdateLowGlobalKrnl.setArg(n_arg++, nBuckets));
            OCL_CHECK(ret, ret = UpdateLowGlobalKrnl.setArg(n_arg++, d_KeyEnd));
            OCL_CHECK(ret, ret = UpdateLowGlobalKrnl.setArg(n_arg++, d_NextBucket));
            OCL_CHECK(ret, ret = UpdateLowGlobalKrnl.setArg(n_arg++, d_LowGlobCurve));
            OCL_CHECK(ret, ret = q.enqueueNDRangeKernel(UpdateLowGlobalKrnl, 0, global_size, local, NULL, NULL));
            OCL_CHECK(ret, ret = q.enqueueReadBuffer(d_LowGlobCurve, CL_TRUE, 0, nBuckets*sizeof(int), h_LowGlobCurve.data(), NULL, NULL));
//			for (auto x : h_LowGlobCurve) {
//				printf("%d ", x);
//			}
//			printf("\n");
        }
    }

    OCL_CHECK(ret, ret = q.enqueueMigrateMemObjects(outBufVec, CL_MIGRATE_MEM_OBJECT_HOST));
    clock_gettime( CLOCK_REALTIME, &enqueue_stop);

    double accum;
	accum = ( enqueue_stop.tv_sec - enqueue_start.tv_sec ) + (double)(( enqueue_stop.tv_nsec - enqueue_start.tv_nsec ) / BILLION);
	printf("Enqueue Time: %f\n", accum);

    printf("Finishing...\n");
    OCL_CHECK(ret, ret = q.finish());
	printf("\nFinished.\n");

    for (int i = 1; i < nBuckets; ++i) {
        if (h_UppGlobCurve[i] == 0 || h_UppGlobCurve[i] < h_UppGlobCurve[i-1]) {
            h_UppGlobCurve[i] = h_UppGlobCurve[i-1];
        }
    }

    for (int i = nBuckets-1; i > 0; --i) {
        if (h_LowGlobCurve[i-1] > h_LowGlobCurve[i]) {
            h_LowGlobCurve[i-1] = h_LowGlobCurve[i];
        }
    }

    clock_gettime( CLOCK_REALTIME, &stop);
    accum = ( stop.tv_sec - start.tv_sec ) + (double)(( stop.tv_nsec - start.tv_nsec ) / BILLION);

//    printf("eventWindows, eventMin, eventMax \n");
//    for (int i = 0; i < nBuckets; ++i) {
//        printf("%li, %li, %li\n", i*r, h_LowGlobCurve[i], h_UppGlobCurve[i]);
//    }
    printf ("%li, %li, %f\n", N, r, accum);

    return SUCCESS;
}

int main(int argc, char **argv) {
    int opt = 0;

    long N = 0;
    long r = 1;
    char *strTraceFileName = NULL;

    while ((opt = getopt(argc, argv, "f:N:r:")) != -1) {
        switch(opt) {
            case 'f':
                strTraceFileName = optarg;
                break;

            case 'N':
                N = atol(optarg);
                break;

            case 'r':
                r = atol(optarg);
                break;

            case '?':
                if (optopt == 'f') {
                    printf("\nMissing file name");
                } else if (optopt == 'r') {
                    printf("\nMissing resolution");
                } else if (optopt == 'N') {
                    printf("\nMissing trace length");
                } else {
                    printf("\nInvalid option received");
                }
                break;
        }
    }

    vector<vector<long> > h_trace_all;
    vector<long> valueline;

    ifstream fin(strTraceFileName);
    if (!fin) {
        printf("No valid input file... Aborting\n");
        return 1;
    }

    string item;
    for (string line; getline(fin, line); ) {
        istringstream in(line);
        while (getline(in, item, '\n')) {
            valueline.push_back(atof(item.c_str()));
        }

        h_trace_all.push_back(valueline);
        valueline.clear();
    }

    if ( N == 0 || N > h_trace_all.size() ) {
        N = h_trace_all.size();
    }

    vector<long, aligned_allocator<long> > h_trace(N);
    
    //Subtract base here and verify whether it's sorted. Sanity check. Exit if not passed.
    for (int j = 0; j < N; ++j) {
        h_trace[j] = h_trace_all[j][0] - h_trace_all[0][0];
//        printf("%d ", h_trace[j]);
    }
    printf("\n");

    return processTrace(h_trace, N, r);
}

