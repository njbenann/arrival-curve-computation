#include <algorithm>
#include <numeric>
#include <fstream>
#include <iostream>     // std::cout
#include <sstream>
#include <string>
#include <time.h>
#include <helper_string.h>
#include <vexcl/vector.hpp>

#include <vexcl/vector.hpp>
#include <vexcl/function.hpp>
#include <boost/iterator/counting_iterator.hpp>

#include <boost/timer/timer.hpp>

#define MILLION 1E6

using namespace std;

void printVector (long n, vector<long>& vector) {
	for (int i = 0; i < n; ++i) {
		printf("%li ", vector[i]);
	}
	printf("\n");
}

const char getQuantInt_kernel_src[] = VEX_STRINGIZE_SOURCE(
	kernel void getQuantInt(ulong n, __global long *trace, __global long *QuantInt, long pivot, long stepSize, ulong shift) {
    	for(uint id = get_global_id(0); id < n; id += get_global_size(0)) {
        	QuantInt[id] = (trace[id + shift] - pivot)/stepSize;
   		}
	}
);

const char GetBucketsAndUpdateUppGlobal_kernel_src[] = VEX_STRINGIZE_SOURCE(
	kernel void GetBucketsAndUpdateUppGlobal(ulong n, __global long *QuantInt, __global long *UppGlobalCurve, __global long *KeyEnd, __global long *NextBucket) {
		__local int current_val, previous_val;

		for (size_t i = get_global_id(0); i < n; i += get_global_size(0)) {
			if (n == 1) {
				KeyEnd[0] = -10;
			}
		
			if (i > 0) {				
				current_val = QuantInt[i];
				previous_val = QuantInt[i-1];
				if (current_val != previous_val) {
					KeyEnd[previous_val] = i-1;
					NextBucket[previous_val] = current_val;
					if (UppGlobalCurve[previous_val] < KeyEnd[previous_val]+1) {
						UppGlobalCurve[previous_val] = KeyEnd[previous_val]+1;
					}
				}
		
				if (i == n-1) {
						KeyEnd[current_val] = -10; //i;
						NextBucket[current_val]= current_val+1;
    				if (UppGlobalCurve[current_val] < i+1) {
						UppGlobalCurve[current_val] = i+1;
    				}
				}
			}
		}
	}
);

const char UpdateUppGlobalR1_kernel_src[] = VEX_STRINGIZE_SOURCE(
	kernel void UpdateUppGlobalR1(ulong n, __global long *QuantInt, __global long *UppGlobalCurve) {
		__local int current_val, previous_val, local_value;

		for(size_t i = get_global_id(0); i < n; i += get_global_size(0))
		{
			if (n == 1)
			local_value = 0;
		
			if (i > 0) {				
				current_val = QuantInt[i];
				previous_val = QuantInt[i-1];
				if (current_val != previous_val) {
					local_value = i-1;
					if (UppGlobalCurve[previous_val] < local_value+1) {
						UppGlobalCurve[previous_val] = local_value+1;
					}
				}
		
				if (i == n-1) {
						local_value = i;
    				if (UppGlobalCurve[current_val] < local_value+1) {
						UppGlobalCurve[current_val] = local_value+1;
    				}
				}
			}
		}
	}
);

const char UpdateLowGlobal_kernel_src[] = VEX_STRINGIZE_SOURCE(
	kernel void UpdateLowGlobal(ulong n, __global long *KeyEnd, __global long *NextBucket, __global long *LowGlobalCurve) {
		__local int first_index, last_index, LocalMin;

		LowGlobalCurve[0] = 0;			
		for (size_t i = get_global_id(0); i < n; i += get_global_size(0)) {
			if (KeyEnd[i] >= 0) {
				LocalMin = KeyEnd[i];
				first_index = i+1;
				last_index = NextBucket[i];
				if (LowGlobalCurve[first_index] > LocalMin) {
					LowGlobalCurve[first_index] = LocalMin;
				}
			
				if (first_index < last_index) {
					if (LowGlobalCurve[last_index] > LocalMin) {
						LowGlobalCurve[last_index] = LocalMin;
					}
				}
			}
		}
	}
);

const char UpdateLowGlobalR1_kernel_src[] = VEX_STRINGIZE_SOURCE(
	kernel void UpdateLowGlobalR1(ulong n, __global long *QuantInt, __global long *LowGlobalCurve) {
		__local int first_index, last_index, LocalMin;
		__local int i, j;

		for (i = get_global_id(0); i < n; i += get_global_size(0)) {
			LocalMin = i;
			first_index = QuantInt[i];
				
			if (i == n-1) {
				last_index = first_index;
			} else {
				last_index = QuantInt[i+1];
			}
				
			for (j = first_index; j < last_index; ++j) {
				if (LowGlobalCurve[j] > LocalMin) {
					LowGlobalCurve[j] = LocalMin;					
				}
			}
		}
	}
);


int main(int argc, char **argv) {
	size_t N = 0;
	bool verbose = 1;
	long stepSize = 1;
	long DtMax = -10;
	long i, j, k;
	char *in_fname = NULL;
	int threads;
	int MIN_THREADS = 1000;

   	vector<vector<long> > h_trace_total;
    vector<long> valueline;

    if (argc > 1) {
        if (checkCmdLineFlag(argc, (const char **) argv, "N")) {
            N = getCmdLineArgumentInt(argc, (const char **)argv, "N");
        }

        if (checkCmdLineFlag(argc, (const char **) argv, "r")) {
        	stepSize = getCmdLineArgumentInt(argc, (const char **)argv, "r");
        }

        if (checkCmdLineFlag(argc, (const char **) argv, "f")) {
        	getCmdLineArgumentString(argc, (const char **)argv, "f", (char **) &in_fname);
        }

       	if (checkCmdLineFlag(argc, (const char **) argv, "DtMax")) {
            DtMax = getCmdLineArgumentInt(argc, (const char **)argv, "DtMax");
        }

        if (checkCmdLineFlag(argc, (const char **) argv, "verbose")) {
        	verbose = getCmdLineArgumentInt(argc, (const char **)argv, "verbose");	
        }
	}

	ifstream fin(in_fname);
	if (!fin) {
		printf("No valid input file... Aborting\n");
		return 1;
	}

	string item;
   	for (string line; getline(fin, line); ) {
       	istringstream in(line);
       	while (getline(in, item, '\n')) {
            valueline.push_back(atof(item.c_str()));
        }

       	h_trace_total.push_back(valueline);
       	valueline.clear();
   	}

	if (N == 0 || N> h_trace_total.size()) {
    	N = h_trace_total.size();
	}

	vector<long> h_trace(N);
	
	//Substract base here and verify whether it's sorted. Sanity check. Exit if not passed.
	for (j = 0; j < N; ++j) {
		h_trace[j] = h_trace_total[j][0] - h_trace_total[0][0] ;
		if ( j > 0 && h_trace[j] < h_trace[j-1]) {
			printf("\n Error: Timestamps in trace file are out of order. TS[%li] < TS[%li]\n", j, j-1);
			return 1;
		}
	}


	// Init VexCL context: grab one GPU with double precision.
	vex::Context ctx(
		vex::Filter::Type(CL_DEVICE_TYPE_GPU) &&
		vex::Filter::Count(1)
	);

	if (ctx.queue().empty()) {
	    cerr << "GPUs with double precision not found." <<endl;
	    return 1;
	}

	//definition of functions and kernels
	vex::backend::kernel getQuantInt_kernel(ctx.queue(0), getQuantInt_kernel_src, "getQuantInt");
	vex::backend::kernel GetBucketsAndUpdateUppGlobal_kernel(ctx.queue(0), GetBucketsAndUpdateUppGlobal_kernel_src, "GetBucketsAndUpdateUppGlobal");
	vex::backend::kernel UpdateLowGlobal_kernel(ctx.queue(0), UpdateLowGlobal_kernel_src, "UpdateLowGlobal");

	vex::backend::kernel UpdateUppGlobalR1_kernel(ctx.queue(0), UpdateUppGlobalR1_kernel_src, "UpdateUppGlobalR1");
	vex::backend::kernel UpdateLowGlobalR1_kernel(ctx.queue(0), UpdateLowGlobalR1_kernel_src, "UpdateLowGlobalR1");

	if (DtMax == -10) {
		DtMax = h_trace[N-1] - h_trace[0];
	}

	long nBuckets = (DtMax/stepSize)+1;

	vector<long> h_QuantInt(N);
	vector<long> h_UppGlobCurve(nBuckets);
	vector<long> h_LowGlobCurve(nBuckets);
	vector<long> h_KeyEnd(nBuckets);
	vector<long> h_NextBucket(nBuckets);	

	long h_pivot;

	vex::vector<long> d_trace(ctx, h_trace);
	vex::vector<long> d_QuantInt(ctx, N);
	vex::vector<long> d_UppGlobCurve(ctx, nBuckets);
	vex::vector<long> d_LowGlobCurve(ctx, nBuckets);
	vex::vector<long> d_KeyEnd(ctx, nBuckets);
	vex::vector<long> d_NextBucket(ctx, nBuckets);

	d_UppGlobCurve = 0;
	d_LowGlobCurve = N+10;

	ctx.finish();

	struct timespec start, stop;
  	clock_gettime(CLOCK_REALTIME, &start);

	for (i = 0; i < N; ++i) {
		h_pivot = h_trace[i];
		getQuantInt_kernel(ctx.queue(0), static_cast<cl_ulong>(N-i), d_trace(0), d_QuantInt(0), h_pivot, stepSize, static_cast<cl_ulong>(i));
		if (stepSize == 1) {
			UpdateUppGlobalR1_kernel(ctx.queue(0), static_cast<cl_ulong>(N-i), d_QuantInt(0), d_UppGlobCurve(0));
			UpdateLowGlobalR1_kernel(ctx.queue(0), static_cast<cl_ulong>(N-i), d_QuantInt(0), d_LowGlobCurve(0));	
		} else {
			d_KeyEnd = -10;
			GetBucketsAndUpdateUppGlobal_kernel(ctx.queue(0), static_cast<cl_ulong>(N-i), d_QuantInt(0), d_UppGlobCurve(0), d_KeyEnd(0), d_NextBucket(0) );
			UpdateLowGlobal_kernel(ctx.queue(0), static_cast<cl_ulong>(nBuckets), d_KeyEnd(0), d_NextBucket(0), d_LowGlobCurve(0));
		}
	}

	vex::copy(d_UppGlobCurve, h_UppGlobCurve);
	vex::copy(d_LowGlobCurve, h_LowGlobCurve);

	for (i = 1; i < nBuckets; ++i) {
		if (h_UppGlobCurve[i] == 0 || h_UppGlobCurve[i] < h_UppGlobCurve[i-1]) {
			h_UppGlobCurve[i] = h_UppGlobCurve[i-1];
		}
	}

    for (i = nBuckets-1; i > 0; --i) {
		if (h_LowGlobCurve[i-1] > h_LowGlobCurve[i]) {
			h_LowGlobCurve[i-1] = h_LowGlobCurve[i];
		}
	}
	
	clock_gettime(CLOCK_REALTIME, &stop);
	double accum;
	accum = ( stop.tv_sec - start.tv_sec ) + (double)(( stop.tv_nsec - start.tv_nsec ) / MILLION);

	printf("eventWindows, eventMin, eventMax \n");
	for (i = 0; i < nBuckets; ++i) {
		printf("%li, %li, %li\n", i*stepSize, h_LowGlobCurve[i], h_UppGlobCurve[i]);
	}
	printf ("%li, %li, %f\n", N, stepSize, accum);
}
