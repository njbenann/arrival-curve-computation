#!/bin/bash

usage() { echo -e "\nUsage: \n $0 -f <trace file name> [-N <A set of number of events in double quotes(\"\")>] [-r <A set of step sizes in double quotes(\"\")>] [-t <number of runs>] \n -N - Max length of the trace is taken by default \n -r - Assigned to 1 by default \n -t - Assigned to 1 by default \n" 1>&2; now="$(date)"; echo "$now INFO: Finish" >> ./$0.log; exit 1; }

# Default values
N=-1
verbose=0
t=1
r=1

now="$(date)"; echo "$now INFO: Start" >> ./$0.log

# Get the arguments TODO: check for double quotes
while getopts ":f:N:r:t:" o; do
    case "${o}" in
        f)
            f=${OPTARG}
            ;;
        N)
            N=""
            N+=${OPTARG}
            ;;
        r)	
			r=""
            r+=${OPTARG}
            ;;
        verbose)
            verbose=${OPTARG}
            ;;
        t)
            t=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))


# Ensure file name is not empty
if [ -z "${f}" ]
then
	now="$(date)"; echo "$now ERROR: Trace file name not given" >> ./$0.log
	echo "$now ERROR: Trace file name not given"
    usage
fi

# Ensure trace file exists
if [ ! -f $f ]
then
   	now="$(date)"; echo "$now ERROR: Trace file does not exist" >> ./$0.log
	echo "$now ERROR: Trace file does not exist"
	echo "$now INFO: Finish" >> ./$0.log
	exit
fi

ver=0

# Choose version
echo "Which version?"
select ch in "OpenCL in GPU" "C in CPU"; do
    case $ch in
        "OpenCL in GPU" ) now="$(date)"; echo "$now INFO: OpenCL in GPU selected"  >> ./$0.log; cd OpenCL-GPU/; ver=1; break;;
        "C in CPU" ) now="$(date)"; echo "$now INFO: C in CPU selected"  >> ./$0.log; cd plainC/; ver=2; break;;
    esac
done  

# Check if data folder exists
# if [ -d data ]
# then
# 	now="$(date)"; echo "$now WARNING: ./data directory exists"  >> ./$0.log
# 	echo "./data directory exists. The script will overrite the contents of the directory. Do you wish to continue?"
# 	select yn in "Yes" "No"; do
# 	    case $yn in
# 	        "Yes" ) now="$(date)"; echo "$now INFO: Deleting ./data directory"  >> ./$0.log; rm -rf data; break;;
# 	        "No" ) now="$(date)"; echo "$now INFO: ./data directory exists - User aborted"  >> ./$0.log; exit;;
# 	    esac
# 	done  
# fi
# mkdir data
# now="$(date)"; echo "$now INFO: ./data directory created"  >> ./$0.log;

if [ -d data ]
then
	now="$(date)"; echo "$now WARNING: ./data directory exists"  >> ./$0.log
else
	mkdir data
	now="$(date)"; echo "$now INFO: ./data directory created"  >> ./$0.log;
fi

# Compile - for now just OpenCL GPU
set -e
now="$(date)"; echo "$now INFO: Building"  >> ./$0.log;
make clean
make -s -f makefile
if [ $? -eq 0 ]; then
    now="$(date)"; echo "$now INFO: Build succeeded"  >> ./$0.log;
else
	echo "$now ERROR: Build failed"; echo now="$(date)"; echo "$now ERROR: Build failed"  >> ./$0.log; exit;
fi
cd ..

if [ "$ver" == "1" ]
then
	if [ "$N" == "-1" ]
	then
		now="$(date)"; echo "$now INPUTS: File: $f, N: Max Length, r: $r, No. of runs: $t, verbose: $verbose"  >> ./$0.log
		file_now="$(date +%F-%T)"
		for step in $r;
		do
			for (( _t=1; _t<=t; ++_t ));
			do
				now="$(date)"; echo "$now RUN: $_t for max length and r=$step"
				echo "$now RUN: $_t for max length and r=$r" >> ./$0.log
				now="$(date +%F-%T)"
				./OpenCL-GPU/AC_OpenCL f=$f r=$step verbose=$verbose >> ./OpenCL-GPU/data/"$f"_"$file_now"_opencl_perf_data.csv
				sleep 10
			done
		done
	else
		now="$(date)"; echo "$now INPUTS: File: $f, N: $N, r: $r, No. of runs: $t, verbose: $verbose"  >> ./$0.log
		file_now="$(date +%F-%T)"
		for n in $N;
		do
			for step in $r;
			do
				for (( _t=1; _t<=t; ++_t ));
				do
					now="$(date)"; echo "$now RUN: $_t for N=$n and r=$step"
					echo "$now RUN: $_t for N=$n and r=$r" >> ./$0.log
					now="$(date +%F-%T)"
					./OpenCL-GPU/AC_OpenCL f=$f N=$n r=$step verbose=$verbose >> ./OpenCL-GPU/data/"$f"_"$file_now"_opencl_perf_data.csv
					sleep 10
				done
			done
		done
	fi
else
	if [ "$N" == "-1" ]
	then
		now="$(date)"; echo "$now INPUTS: File: $f, N: Max Length, r: $r, No. of runs: $t, verbose: $verbose"  >> ./$0.log
		file_now="$(date +%F-%T)"
		for step in $r;
		do
			for (( _t=1; _t<=t; ++_t ));
			do
				now="$(date)"; echo "$now RUN: $_t for max length and r=$step"
				echo "$now RUN: $_t for max length and r=$r" >> ./$0.log
				now="$(date +%F-%T)"
				./plainC//AC-plainC f=$f r=$step verbose=$verbose >> ./plainC/data/"$f"_"$file_now"_plainC_perf_data.csv
				sleep 10
			done
		done
	else
		now="$(date)"; echo "$now INPUTS: File: $f, N: $N, r: $r, No. of runs: $t, verbose: $verbose"  >> ./$0.log
		file_now="$(date +%F-%T)"
		for n in $N;
		do
			for step in $r;
			do
				for (( _t=1; _t<=t; ++_t ));
				do
					now="$(date)"; echo "$now RUN: $_t for N=$n and r=$step"
					echo "$now RUN: $_t for N=$n and r=$r" >> ./$0.log
					./plainC//AC-plainC f=$f N=$n r=$step verbose=$verbose >> ./plainC/data/"$f"_"$file_now"_plainC_perf_data.csv
					sleep 10
				done
			done
		done
	fi
fi

now="$(date)"; echo "$now INFO: Finish" >> ./$0.log
